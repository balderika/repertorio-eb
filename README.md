B, 'Erika':                                                                                                                                     
Milpixqui y Psicóloga clínica (UNAM,MX).

🥢🚲🍍🐔🐱🏞️🧶🎲💻🎮🎹📽️

#Aquaponics #AppliedBehaviorAnalysis #ClinicalPsychology

💼 https://gitlab.com/balderika/repertorio-eb/-/tree/master/repertorio-%20EBM                                                                    
📠 https://mstdn.io/@erikabalmed                                                                                                                
🙈 https://twitter.com/balderikas/likes                                                                                                         
🙋‍ Facebook: erika.psiclinic

